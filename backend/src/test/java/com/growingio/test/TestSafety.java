package com.growingio.test;

import com.growingio.backend.ServerRoomSafety;
import org.junit.Test;

import java.io.*;
import java.util.*;

/**
 * 后端单元测试
 *
 * @author Roah
 * @since 04/08/2018
 */
public class TestSafety {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Integer num = sc.nextInt();
        Integer[] height = new Integer[num];
        for (int i = 0; i < num; i++) {
            height[i]=sc.nextInt();
        }
        Integer[] position = new Integer[num];
        for (int j = 0; j < num; j++) {
            position[j]=sc.nextInt();
        }
        System.out.println(ServerRoomSafety.fall(num,height,position));
    }
    @Test
    public void testDefault() throws Exception {
        StringBuffer inputfileName = new StringBuffer();
        StringBuffer outputfileName = new StringBuffer();
        ArrayList<String> result = new ArrayList<String>();
        for(Integer i=0;i<=14;i++){
            outputfileName.setLength(0);
            outputfileName.append("/testdata/output/").append("output");
            if(i.intValue()<10){
                outputfileName.append(0).append(i.toString()).append(".txt");
            }else{
                outputfileName.append(i.toString()).append(".txt");
            }

            File file = new File(ServerRoomSafety.class.getResource(outputfileName.toString()).getFile());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String len = null;
            while ((len=br.readLine())!=null){
                result.add(len);
            }
        }

        for(Integer i =0;i<=14;i++){
            inputfileName.setLength(0);
            inputfileName.append("/testdata/input/").append("input");
            if(i.intValue()<10){
                inputfileName.append(0).append(i.toString()).append(".txt");
            }else{
                inputfileName.append(i.toString()).append(".txt");
            }
            File file = new File(ServerRoomSafety.class.getResource(inputfileName.toString()).getFile());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String len = null;
            int j = 0;
            int length = 0;
            ArrayList<Integer> height = new ArrayList<Integer>();
            ArrayList<Integer> position = new ArrayList<Integer>();
            while ((len=br.readLine())!=null){
                if(j == 0){
                    len=len.trim();
                    length = Integer.parseInt(len);
                }else if(j==1){
                    String[] resTest = len.split(" ");
                    for(String str : resTest) {
                        int toIntInfo = Integer.parseInt(str);
                        height.add(toIntInfo);
                    }
                }else if(j==2){
                    String[] resTest = len.split(" ");
                    for(String str : resTest) {
                        int toIntInfo = Integer.parseInt(str);
                        position.add(toIntInfo);
                    }
                }else{
                    break;
                }
                j++;
            }
            System.out.println(length+height.toString()+position.toString());
           /* System.out.println(ServerRoomSafety.fall(length,height.toArray(),position));
            if(ServerRoomSafety.fall(length,height,position).equals(result.get(i))){
                System.out.println("测试成功");
            }else{
                System.out.println("测试失败");
            }*/

        }
    }
}