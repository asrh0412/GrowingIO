package com.growingio.backend;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * 具体方法类
 *
 * @author Roah
 * @since 04/08/2018
 */
public class ServerRoomSafety {
    /*测试主类*/
    public static String fall(int n, Integer[] height, Integer[] position) {
        if (n == 1) {
            return "BOTH";
        }

        boolean leftSide = false;
        boolean rightSide = false;

        int i = 0;
        int max = height[0] + position[0];
        for (; i < position.length - 1; i++) {
            if (height[i] + position[i] > max) {
                max = height[i] + position[i];
            }

            if (max < position[i + 1]) {
                leftSide = true;
                break;
            }

        }
        i = n - 1;
        int min = position[n - 1] - height[n - 1];
        for (; i > 0; i--) {
            if (position[i] - height[i] < min) {
                min = position[i] - height[i];
            }

            if (min > position[i - 1]) {
                rightSide = true;
                break;
            }

        }
        return returnResult(leftSide, rightSide);
    }

    /*测试返回结果*/
    static String returnResult(Boolean leftSide, Boolean rightSide) {
        String res = null;
        if (rightSide && leftSide) {
            res = "NONE";
        } else if (!rightSide && !leftSide) {
            res = "BOTH";
        } else if (!leftSide) {
            res = "LEFT";
        } else if (!rightSide) {
            res = "RIGHT";
        } else
            return "ERROR";
        return res;
    }
}
